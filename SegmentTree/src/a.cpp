#include <bits/stdc++.h>

using namespace std;

using LL = long long;

int n, m;
int num;

vector< vector<int> > childs;
vector<int> weight, foundation;

vector<int> allSons;
vector<int> indexs;

typedef struct Node *pNode;

struct Node {
    LL sum;
    LL lazy;
    pNode lchs, rchs;

    Node(LL sum = 0, LL lazy = 0, pNode lchs = nullptr, pNode rchs = nullptr) : 
        sum(sum),
        lazy(lazy),
        lchs(lchs),
        rchs(rchs) {}
    ~Node() {
        if (lchs != nullptr)
            delete lchs;
        if (rchs != nullptr)
            delete rchs;
    }
};

int dfs(int root)
{
    indexs[root] = num++;
    allSons[root] = 0;
    for (auto to : childs[root]) {
        allSons[root] += dfs(to);
    }
    return allSons[root] + 1;
}

void build(pNode &root, int l, int r)
{
    root = new Node;
    if (l + 1 == r) {
        root->sum = weight[foundation[l]];
        return ;
    }
    
    int mid = (l + r) >> 1;
    build(root->lchs, l, mid);
    build(root->rchs, mid, r);
    
    root->sum = root->lchs->sum + root->rchs->sum;
}

LL query(pNode root, int l, int r, int a, int b)
{
    root->sum += (r - l) * root->lazy;
    if (r - l != 1) {
        root->lchs->lazy += root->lazy;
        root->rchs->lazy += root->lazy;
    }
    root->lazy = 0;

    if (l == a && r == b)
        return root->sum;

    int mid = (l + r) >> 1;
    LL ret = 0;
    if (b <= mid)
        ret += query(root->lchs, l, mid, a, b);
    else if (a >= mid)
        ret += query(root->rchs, mid, r, a, b);
    else
        ret += query(root->lchs, l, mid, a, mid) +
                query(root->rchs, mid, r, mid, b);
    return ret;
}

bool update(pNode root, int l, int r, int a, int b, int tag)
{
    if (l == a && r == b) {
        root->lazy += tag;
        return true;
    }
    
    root->sum += (b - a) * tag;
    int mid = (l + r) >> 1;
    if (b <= mid)
        update(root->lchs, l, mid, a, b, tag);
    else if (a >= mid)
        update(root->rchs, mid, r, a, b, tag);
    else
        update(root->lchs, l, mid, a, mid, tag),
        update(root->rchs, mid, r, mid, b, tag);
    return true;
}

int main()
{
    for (; EOF != scanf("%d%d", &n, &m); ) {
        childs.clear();
        childs.resize(n + 1);
        weight.resize(n + 1);
        weight[1] = 0;
        allSons.resize(n + 1, 0);
        indexs.resize(n + 1);
        foundation.resize(n + 1);

        for (int i = 2, w, p; i <= n; ++i) {
            scanf("%d%d", &p, &w);
            childs[p + 1].push_back(i);
            weight[i] = w;
        }

        num = 1, dfs(1);
        for (int i = 1; i < n + 1; foundation[indexs[i]] = i, ++i) {}

        pNode root = nullptr;
        build(root, 1, n + 1);
        for ( ; m--; ) {
            char C[2];
            int x, y, z;
            scanf("%s%d%d%d", C, &x, &y, &z);
            ++x;
            if (C[0] == 'S') {
                query(root, 1, n + 1, indexs[x], indexs[x] + 1) < 1LL * y ? update(root, 1, n + 1, indexs[x], indexs[x] + 1, z) : true;
            } else {
                query(root, 1, n + 1, indexs[x], indexs[x] + allSons[x] + 1) < 1LL * y * allSons[x] + y ? update(root, 1, n + 1, indexs[x], indexs[x] + allSons[x] + 1, z) : true;
            }
        }
        
        for (int i = 1; i <= n; ++i)
            printf("%lld\n", query(root, 1, n + 1, indexs[i], indexs[i] + 1));

        if (root != nullptr)
            delete root;
    }
    return 0;
}
