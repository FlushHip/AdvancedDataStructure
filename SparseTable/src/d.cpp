#include <bits/stdc++.h>

using namespace std;

#define MAX_SIZE (100000 + 10)
#define TOP (int)(log(MAX_SIZE) / log(2)) + 1

typedef long long LL;

int arr[MAX_SIZE];
int dp[MAX_SIZE][TOP];
int Log2[MAX_SIZE];
int n;

int gcd(int a, int b)
{
    return b == 0 ? a : gcd(b, a % b);
}

// BF : binary_function
typedef int (*BF) (int, int);

void init()
{
    for (int i = 1; i < MAX_SIZE; ++i)
        Log2[i] = Log2[i >> 1] + 1;
}

void pretreat(BF func)
{
    for (int left = 0; left < n; dp[left][0] = arr[left], ++left) {}
    for (int i = 1; i < Log2[n]; ++i)
        for (int left = 0; left + (1 << i) <= n; ++left)
            dp[left][i] = func(dp[left][i - 1], dp[left + (1 << (i - 1))][i - 1]);
}

// [l, r)
int query(int l, int r, BF func)
{
    return func(dp[l][Log2[r - l] - 1], dp[r - (1 << Log2[r - l] - 1)][Log2[r - l] - 1]);
}

int bFind(int src, int l, int r, int tag)
{
    while (l < r) {
        int mid = (l + r) >> 1;
        if (query(src, mid + 1, gcd) == tag)
            l = mid + 1;
        else
            r = mid;
    }
    return l;
}

int main()
{
    init();
    int T, K = 1, q;
    for (scanf("%d", &T); T--; ++K) {
        scanf("%d", &n);
        for (int i = 0; i < n; scanf("%d", &arr[i++])) {}
        scanf("%d", &q);
        BF pFunc = gcd;
        pretreat(pFunc);

        printf("Case #%d:\n", K);

        int ans1;
        map<int, LL> ans2;

        for (int src = 0; src < n; ++src) {
            for (int left = src, right; left < n; left = right) {
                int g = query(src, left + 1, pFunc);
                right = bFind(src, left, n, g);
                ans2[g] += right - left;
            }
        }

        for (int l, r; q--; ) {
            scanf("%d%d", &l, &r);
            ans1 = query(l - 1, r, pFunc);
            printf("%d %lld\n", ans1, ans2[ans1]);
        }
    }
    return 0;
}
