#include <bits/stdc++.h>

using namespace std;

#define MAX_SIZE (1 << 10)
#define TOP (int)(log(MAX_SIZE) / log(2)) + 1

int arr[MAX_SIZE];
int dp[MAX_SIZE][TOP];
int Log2[MAX_SIZE];
int n;

int min(int a, int b)
{
	return a < b ? a : b;
}

int max(int a, int b)
{
	return a > b ? a : b;
}

int gcd(int a, int b)
{
	return b == 0 ? a : gcd(b, a % b);
}

// BF : binary_function
typedef int (*BF) (int, int);

void init()
{
	for (int i = 1; i < MAX_SIZE; ++i)
		Log2[i] = Log2[i >> 1] + 1;
}

void pretreat(BF func)
{
	for (int left = 0; left < n; dp[left][0] = arr[left], ++left) {}
	for (int i = 1; i < Log2[n]; ++i)
		for (int left = 0; left + (1 << i) <= n; ++left)
			dp[left][i] = func(dp[left][i - 1], dp[left + (1 << (i - 1))][i - 1]);
}

// [l, r)
int query(int l, int r, BF func)
{
	return func(dp[l][Log2[r - l] - 1], dp[r - (1 << Log2[r - l] - 1)][Log2[r - l] - 1]);
}

int main()
{
	init();
	for (; EOF != scanf("%d", &n); ) {
		for (int i = 0; i < n; scanf("%d", &arr[i++])) {}
		// arr[0] = 2; arr[1] = 1; arr[2] = 4; arr[3] = 3; arr[4] = 5;
		BF pFunc = min;
		pretreat(pFunc);
		for (int l, r; ; ) {
			scanf("%d%d", &l, &r);
			assert(l < r);
			assert(0 <= l && l <= n);
			assert(0 <= r && r <= n);
			printf("[%d, %d) : %d\n", l, r, query(l, r, pFunc));
		}
	}
	return 0;
}
