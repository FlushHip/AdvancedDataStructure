#include <stdio.h>
#include <utility>
#include <map>

using namespace std;

#define MAX_SIZE (100000 + 10)
#define TOP 20

int a[MAX_SIZE];
int arr[MAX_SIZE];
int dp[MAX_SIZE][TOP];
int Log2[MAX_SIZE];
int n;

int max(int a, int b)
{
	return a > b ? a : b;
}

// BF : binary_function
typedef int (*BF) (int, int);

void init()
{
	for (int i = 1; i < MAX_SIZE; ++i)
		Log2[i] = Log2[i >> 1] + 1;
}

void pretreat(BF func)
{
	for (int left = 0; left < n; dp[left][0] = arr[left], ++left) {}
	for (int i = 1; i < Log2[n]; ++i)
		for (int left = 0; left + (1 << i) <= n; ++left)
			dp[left][i] = func(dp[left][i - 1], dp[left + (1 << (i - 1))][i - 1]);
}

// [l, r)
int query(int l, int r, BF func)
{
	return func(dp[l][Log2[r - l] - 1], dp[r - (1 << Log2[r - l] - 1)][Log2[r - l] - 1]);
}

int main()
{
	init();
	for (int q; EOF != scanf("%d", &n) && n; ) {
        scanf("%d", &q);
        map<int, int> mp;
        for (int i = 0; i < n; ++i) {
            scanf("%d", &a[i]);
            mp[a[i]] = i;
            arr[i] = i == 0 ? 1 : a[i] == a[i - 1] ? arr[i - 1] + 1 : 1;
        }
        
		BF pFunc = max;
		pretreat(pFunc);
		for (int l, r; q--; ) {
			scanf("%d%d", &l, &r);
            int src = mp[a[l - 1]];         // last-index occur
            if (src >= r - 1)
                printf("%d", r - l + 1);
            else
                // [l - 1, src]...[src + 1, r)
                printf("%d", pFunc(src - (l - 1) + 1, query(src + 1, r, pFunc)));
            puts("");
		}
	}
	return 0;
}
