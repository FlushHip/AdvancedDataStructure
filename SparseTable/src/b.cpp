#include <stdio.h>
#include <math.h>
#include <utility>

using namespace std;

#define MAX_SIZE (50000 + 10)
#define TOP 20

int arr[MAX_SIZE];
pair<int, int> dp[MAX_SIZE][(const int)TOP];
int Log2[MAX_SIZE];
int n;

int min(int a, int b)
{
	return a < b ? a : b;
}

int max(int a, int b)
{
	return a > b ? a : b;
}

pair<int, int> minmax(const pair<int, int> &a, const pair<int, int> &b)
{
    return make_pair(min(a.first, b.first), max(a.second, b.second));
}

// BF : binary_function
typedef pair<int, int> (*BF) (const pair<int, int> &, const pair<int, int> &);

void init()
{
	for (int i = 1; i < MAX_SIZE; ++i)
		Log2[i] = Log2[i >> 1] + 1;
}

void pretreat(BF func)
{
	for (int left = 0; left < n; dp[left][0] = make_pair(arr[left], arr[left]), ++left) {}
	for (int i = 1; i < Log2[n]; ++i)
		for (int left = 0; left + (1 << i) <= n; ++left)
			dp[left][i] = func(dp[left][i - 1], dp[left + (1 << (i - 1))][i - 1]);
}

// [l, r)
pair<int, int> query(int l, int r, BF func)
{
	return func(dp[l][Log2[r - l] - 1], dp[r - (1 << Log2[r - l] - 1)][Log2[r - l] - 1]);
}

int main()
{
	init();
	for (int q; EOF != scanf("%d%d", &n, &q); ) {
		for (int i = 0; i < n; scanf("%d", &arr[i++])) {}
		// arr[0] = 2; arr[1] = 1; arr[2] = 4; arr[3] = 3; arr[4] = 5;
		BF pFunc = minmax;
		pretreat(pFunc);
		for (int l, r; q--; ) {
			scanf("%d%d", &l, &r);
            pair<int, int> ret = query(l - 1, r, pFunc);
            printf("%d\n", ret.second - ret.first);
		}
	}
	return 0;
}
